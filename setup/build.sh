#!/bin/bash

/usr/bin/pip3 install django==1.10.6
/usr/bin/pip3 install psycopg2
/usr/bin/pip3 install mock

# cd "${0%/*}"
cd /var/www/html
git clone https://gitlab.com/PPL2017csui/PPLC3.git
cd PPLC3
git config user.name "PPLC3 - Production"
git config user.email "oda.ryorda@gmail.com"

git checkout develop
git pull

cd lipsy
python3 manage.py collectstatic
rm -rf app/migrations/* app/migrations/__pycache__ app/__pycache__/*

service postgresql start

sudo -u postgres psql -c "DROP DATABASE lipsy"
sudo -u postgres psql -c "ALTER USER postgres WITH PASSWORD 'password'"
sudo -u postgres psql -c "CREATE DATABASE lipsy OWNER postgres"
chown -R www-data /var/www/html/PPLC3/*

wget -O /etc/apache2/sites-available/000-default.conf https://gitlab.com/PPL2017csui/PPLC3/raw/develop/setup/000-default.conf

python3 manage.py flush
python3 manage.py makemigrations
python3 manage.py migrate

echo "Now create your superuser"
python3 manage.py createsuperuser

service apache2 start
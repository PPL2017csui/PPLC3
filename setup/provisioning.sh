#!/bin/bash
cat >Dockerfile <<EOL
FROM ubuntu

RUN apt-get -y update
RUN apt-get -y install apache2 python3 python3-pip postgresql git
RUN apt-get install wget libapache2-mod-wsgi-py3 sudo

RUN /usr/bin/pip3 install django==1.10.6

# Manually set up the apache environment variables
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid

# set port to be exposed
# don't forget to configure port forwarding on your virtualbox

EXPOSE 80

EOL

docker login
docker built pplc3/ui-lipsy:psql .
	
docker stop lipsy
docker rm lipsy
docker run -it -p 80:80 -p 8000:8000 -d --name=lipsy pplc3/ui-lipsy:psql

docker exec lipsy wget -O /var/www/html/build.sh https://gitlab.com/PPL2017csui/PPLC3/raw/develop/setup/build.sh

echo -e "Now, you can deploy app using following code : \n\n\t docker exec lipsy bash /var/www/html/build.sh"

[![coverage](https://gitlab.com/PPL2017csui/PPLC3/badges/develop/coverage.svg)](https://gitlab.com/PPL2017csui/PPLC3/commits/develop)
[![build](https://gitlab.com/PPL2017csui/PPLC3/badges/develop/build.svg)](https://gitlab.com/PPL2017csui/PPLC3/commits/develop)

# Welcome to Lipsy

<h2>Migrating & seeding the database</h2>

python manage.py migrate

python manage.py loaddata lipsydb.json

update seed: python manage.py dumpdata > lipsydb.json
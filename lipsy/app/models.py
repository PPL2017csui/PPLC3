from __future__ import unicode_literals
from django.db import models
import datetime
from django.utils import timezone


class Role(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return str(self.id) + " " + self.name;


class Admin(models.Model):
    nip = models.CharField(max_length=18, primary_key=True)
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    username = models.CharField(max_length=30, default="")
    password = models.CharField(max_length=30, default="")
    email = models.EmailField(max_length=100, default="")
    name = models.CharField(max_length=30, default="")
    token = models.CharField(max_length=100)
    created_at = models.DateTimeField(default=timezone.now, blank=True)
    updated_at = models.DateTimeField(default=timezone.now, blank=True)

    def __str__(self):
        return str(self.nip);


class Level(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return str(self.id) + " " + self.name;


class Faculty(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return str(self.id) + " " + self.name;


class Status(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return str(self.id) + " " + self.name;


class Regulation(models.Model):
    name = models.CharField(max_length=50)
    created_at = models.DateTimeField(default=timezone.now, blank=True)
    updated_at = models.DateTimeField(default=timezone.now, blank=True)

    def __str__(self):
        return str(self.id) + " " + self.name;


class RegulationQuestion(models.Model):
    regulation = models.ForeignKey(Regulation, on_delete=models.CASCADE)
    creator = models.ForeignKey(Admin, on_delete=models.CASCADE)
    question = models.TextField()
    created_at = models.DateTimeField(default=timezone.now, blank=True)
    updated_at = models.DateTimeField(default=timezone.now, blank=True)

    def __str__(self):
        return str(self.id) + " " + self.question;


class CalonYangDiusulkan(models.Model):
    nip = models.CharField(max_length=18, unique=True)
    name = models.CharField(max_length=50)
    birthdate = models.DateTimeField(default=timezone.now, blank=True)
    birthplace = models.CharField(max_length=30)
    current_level = models.ForeignKey(Level, on_delete=models.CASCADE)
    email = models.CharField(max_length=50)
    faculty = models.ForeignKey(Faculty, on_delete=models.CASCADE)
    gender = models.CharField(max_length=1)
    created_at = models.DateTimeField(default=timezone.now, blank=True)
    updated_at = models.DateTimeField(default=timezone.now, blank=True)

    def __str__(self):
        return str(self.id) + " " + self.name;


class Application(models.Model):
    cyd = models.ForeignKey(CalonYangDiusulkan, on_delete=models.CASCADE)
    status = models.ForeignKey(Status, on_delete=models.CASCADE)
    regulation = models.ForeignKey(Regulation, on_delete=models.CASCADE)
    destination_level = models.ForeignKey(Level, on_delete=models.CASCADE)
    created_at = models.DateTimeField(default=timezone.now, blank=True)
    updated_at = models.DateTimeField(default=timezone.now, blank=True)

    def __str__(self):
        return str(self.id) + " " + str(self.cyd.nip);


class EducationHistory(models.Model):
    cyd = models.ForeignKey(CalonYangDiusulkan, on_delete=models.CASCADE)
    name = models.TextField()
    order = models.IntegerField()

    def __str__(self):
        return str(self.id) + " " + self.name;


class StatusChangeRecord(models.Model):
    cyd = models.ForeignKey(CalonYangDiusulkan, on_delete=models.CASCADE)
    status = models.ForeignKey(Status, on_delete=models.CASCADE)
    description = models.TextField()
    created_at = models.DateTimeField(default=timezone.now, blank=True)

    def __str__(self):
        return str(self.id) + " " + self.description;


class ApplicationHistory(models.Model):
    cyd = models.ForeignKey(CalonYangDiusulkan, on_delete=models.CASCADE)
    description = models.TextField()
    change_record = models.ForeignKey(StatusChangeRecord, on_delete=models.CASCADE, null=True)
    revision_number = models.IntegerField()
    created_at = models.DateTimeField(default=timezone.now, blank=True)

    def __str__(self):
        return str(self.id) + " " + self.description;


class Record(models.Model):
    application_history = models.ForeignKey(ApplicationHistory, on_delete=models.CASCADE)
    regulation_question = models.ForeignKey(RegulationQuestion, on_delete=models.CASCADE)
    record = models.TextField()
    created_at = models.DateTimeField(default=timezone.now, blank=True)

    def __str__(self):
        return str(self.id) + " " + self.record;


class DocumentBundle(models.Model):
    application = models.ForeignKey(Application)
    document = models.FileField(upload_to='berkas/%Y/%m/%d/')
    upload_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id) + " " + self.document.name


class AcceptanceLetter(models.Model):
    application = models.ForeignKey(Application)
    document = models.FileField(upload_to='acc_letter/%Y/%m/%d/')
    upload_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id) + " " + self.document.name


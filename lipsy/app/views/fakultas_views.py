from django.shortcuts import render, redirect
from ..controllers import fakultas_memantau_aplikasi_cyd as fmac
from ..controllers import fakultas_ajukan_berkas as fab
from ..controllers import pagination as p
from django.core.exceptions import ObjectDoesNotExist
from app import models


def fakultas_pantau_aktif(request):
	fakultas = 1
	dikembalikan = fmac.get_cyd(fakultas, 'Dikembalikan')
	dikembalikan_list = fmac.cyd_dikembalikan_list(dikembalikan)
	cyd_list1 = p.pagination(request, 'page1', dikembalikan_list)
	diproses1 = fmac.get_cyd(fakultas, 'Diproses')
	diproses2 = fmac.get_cyd(fakultas, 'Diajukan')
	diproses_list = fmac.cyd_diproses_list(diproses1) + fmac.cyd_diproses_list(diproses2)
	cyd_list2 = p.pagination(request, 'page2', diproses_list)
	return render(request, 'app/fakultas/fakultas_pantau_aktif.html',
				  {'cyd_dikembalikan': cyd_list1, 'cyd_diproses': cyd_list2, "user": {"role": "Fakultas"}})


def fakultas_pantau_dibatalkan(request):
	fakultas = 1
	dibatalkan = fmac.get_cyd(fakultas, 'Dibatalkan')
	dibatalkan_list = fmac.cyd_dibatalkan_list(dibatalkan)
	cyd_list = p.pagination(request, 'page0', dibatalkan_list)
	return render(request, 'app/fakultas/fakultas_pantau_dibatalkan.html',
				  {'cyd_dibatalkan': cyd_list, "user": {"role": "Fakultas"}})


def fakultas_pantau_disetujui(request):
	fakultas = 1
	disetujui = fmac.get_cyd(fakultas, 'Disetujui')
	disetujui_list = fmac.cyd_disetujui_list(disetujui)
	cyd_list = p.pagination(request, 'page3', disetujui_list)
	return render(request, 'app/fakultas/fakultas_pantau_disetujui.html',
				  {'cyd_disetujui': cyd_list, "user": {"role": "Fakultas"}})


def fakultas_upload(req):
	if (req.method == "POST"):
		NIP = req.POST.get('NIP', '-')
		TYPE = req.POST.get('recc_type', 0)
		FILE = req.FILES['file']

		try:
			CYD = models.CalonYangDiusulkan.objects.get(nip=NIP)
		except ObjectDoesNotExist:
			return render(req, 'app/fakultas/upload.html',
						  {"user": {"role": "Fakultas"}, 'error': 'NIP {0} tidak ditemukan'.format(NIP)})

		try:
			models.Application.objects.get(cyd=CYD)
			return render(req, 'app/fakultas/upload.html', {"user": {"role": "Fakultas"},
															'error': 'Aplikasi dengan NIP {0} sudah dikumpulkan'.format(
																NIP)})

		except ObjectDoesNotExist:
			pass

		try:
			fab.fakultas_upload(nip=NIP, file=FILE, recc_type=TYPE)

			return render(req, 'app/fakultas/upload.html', {"user": {"role": "Fakultas"},
															'success': 'Berhasil mengajukan berkas untuk NIP {0}'.format(
																NIP)})

		except fab.LevelExceededException as e:

			return render(req, 'app/fakultas/upload.html', {"user": {"role": "Fakultas"}, 'error': e})

	return render(req, 'app/fakultas/upload.html', {"user": {"role": "Fakultas"}})


def fakultas_upload_revision(req):
	if (req.method == "POST") :
		NIP = req.POST.get('NIP', '-')
		try :
			CYD = models.CalonYangDiusulkan.objects.get(nip=NIP)
			aplikasi = models.Application.objects.get(cyd=CYD)
			if ( "Dikembalikan" not in aplikasi.status.name ) :
				raise Exception("Status aplikasi tidak sesuai")

			if (req.POST.get("confirmation", "no") == "yes"):
				fab.fakultas_ajukan_kembali(nip=NIP, file=req.FILES['file'])
				return redirect( "fakultas_pantau_aktif") 
			else :
				return render(req, 'app/fakultas/fakultas_ajukan_kembali.html', {"user": {"role": "Fakultas"} , "staff" : { "nip" : NIP, "current_level" : CYD.current_level.name , "dest_level" : aplikasi.destination_level.name} } )

		except Exception :
			return render(req, 'app/fakultas/fakultas_ajukan_kembali.html', {"user": {"role": "Fakultas"}, "error" : "NIP {0} tidak dapat melakukan pengajuan ulang ".format(NIP) })

	return render(req, 'app/fakultas/fakultas_ajukan_kembali.html', {"user": {"role": "Fakultas"}, "error" : "Tidak ada NIP yang dipilih untuk diajukan ulang"})
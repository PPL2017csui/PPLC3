from django.shortcuts import render
from ..controllers import dgb_melihat_riwayat_pengajuan as dmrp
from ..controllers import pagination as p


def retrieve_application_history(req):
    accepted_applications = dmrp.get_accepted_applications()
    rejected_applications = dmrp.get_rejected_applications()
    acc_list = p.pagination(req, "acc_page", accepted_applications)
    rej_list = p.pagination(req, "rej_page", rejected_applications)
    return render(req, 'app/dgb/application_history.html',
                  {'accepted_applications': acc_list, "rejected_applications": rej_list,
                   "user": {"role": "DGB"}})
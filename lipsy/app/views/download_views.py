from ..controllers import download_file as df


def download_berkas(request, cyd_id):
    resp = df.download_berkas(cyd_id)
    return resp


def download_acc(request, cyd_id):
    resp = df.download_acc(cyd_id)
    return resp

from django.contrib import admin

from .models import Role, Admin, Level, Faculty, Status, Regulation, RegulationQuestion, CalonYangDiusulkan, Application, EducationHistory, ApplicationHistory, Record, StatusChangeRecord, DocumentBundle, AcceptanceLetter

admin.site.register(Role)
admin.site.register(Admin)
admin.site.register(Level)
admin.site.register(Faculty)
admin.site.register(Status)
admin.site.register(Regulation)
admin.site.register(RegulationQuestion)
admin.site.register(CalonYangDiusulkan)
admin.site.register(Application)
admin.site.register(EducationHistory)
admin.site.register(ApplicationHistory)
admin.site.register(Record)
admin.site.register(StatusChangeRecord)
admin.site.register(DocumentBundle)
admin.site.register(AcceptanceLetter)

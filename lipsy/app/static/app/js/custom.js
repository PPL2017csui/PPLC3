/* disable all enter submit */

$(document).on("keypress", 'form', function (e) {
	var code = e.keyCode || e.which;
	if (code == 13) {
		e.preventDefault();
		return false;
	}
});

function viewLog(name) {
	var log = document.getElementById("log");
	var record = document.getElementById(name);
	log.innerHTML = record.innerHTML;
}
from django.test import TestCase
from django.urls import reverse
from django.test import Client
from django.test.client import RequestFactory
from ..models import DocumentBundle, AcceptanceLetter, CalonYangDiusulkan, Application, Status, Level, Faculty, \
    Regulation, StatusChangeRecord, Admin, Record, Regulation, Role, ApplicationHistory, RegulationQuestion
from ..controllers import fakultas_memantau_aplikasi_cyd as fmac
from django.core.exceptions import ObjectDoesNotExist
from mock import patch
from mock import MagicMock
import mock
import unittest
from django.http import HttpResponse
from django.core.files import File


class HomeFakultasTest(TestCase):
    def create_objects(self):
        role = Role.objects.create(name="DGB")

        admin = Admin.objects.create(nip=1000067891234567, role=role)

        reguler = Regulation.objects.create(name='Reguler')

        teknik = Faculty.objects.create(name='Teknik')
        self.teknik = teknik

        diproses_sdm = Status.objects.create(name='Diproses SDM')
        diajukan_fakultas = Status.objects.create(name='Diajukan Fakultas')
        diproses_dgb = Status.objects.create(name='Diproses DGB')
        dikembalikan = Status.objects.create(name='Dikembalikan SDM')
        dikembalikan_dgb = Status.objects.create(name='Dikembalikan DGB')
        disetujui = Status.objects.create(name='Disetujui DGB')
        dibatalkan = Status.objects.create(name='Dibatalkan')

        lektor = Level.objects.create(name='Lektor')
        lektor_kepala = Level.objects.create(name='Lektor Kepala')

        sumarni = CalonYangDiusulkan.objects.create(nip='111111', name='Sumarni', birthplace='Lamongan',
                                                    current_level=lektor, email='sumarni@abc.com', faculty=teknik,
                                                    gender='P')
        self.sumarni = sumarni
        sukarwo = CalonYangDiusulkan.objects.create(nip='111112', name='Sukarwo', birthplace='Kediri',
                                                    current_level=lektor, email='sukarwo@abc.com', faculty=teknik,
                                                    gender='L')
        self.sukarwo = sukarwo
        ahmad = CalonYangDiusulkan.objects.create(nip='111113', name='Ahmad', birthplace='Malang', current_level=lektor,
                                                  email='ahmad@abc.com', faculty=teknik, gender='L')
        self.ahmad = ahmad
        susan = CalonYangDiusulkan.objects.create(nip='111114', name='Susan', birthplace='Depok', current_level=lektor,
                                                  email='susan@abc.com', faculty=teknik, gender='P')
        self.susan = susan
        nana = CalonYangDiusulkan.objects.create(nip='111115', name='Nana', birthplace='Depok', current_level=lektor,
                                                 email='nana@abc.com', faculty=teknik, gender='P')
        self.nana = nana
        nene = CalonYangDiusulkan.objects.create(nip='111116', name='Nene', birthplace='Depok', current_level=lektor,
                                                 email='nene@abc.com', faculty=teknik, gender='P')
        self.nene = nene
        jarot = CalonYangDiusulkan.objects.create(nip='111117', name='Jarot', birthplace='Depok', current_level=lektor,
                                                  email='jarot@abc.com', faculty=teknik, gender='L')
        self.jarot = jarot

        app_jarot = Application.objects.create(cyd=jarot, status=diajukan_fakultas, regulation=reguler,
                                               destination_level=lektor_kepala)
        app_sumarni = Application.objects.create(cyd=sumarni, status=diproses_sdm, regulation=reguler,
                                                 destination_level=lektor_kepala)
        app_sukarwo = Application.objects.create(cyd=sukarwo, status=dikembalikan_dgb, regulation=reguler,
                                                 destination_level=lektor_kepala)
        app_ahmad = Application.objects.create(cyd=ahmad, status=disetujui, regulation=reguler,
                                               destination_level=lektor_kepala)
        app_susan = Application.objects.create(cyd=susan, status=dibatalkan, regulation=reguler,
                                               destination_level=lektor_kepala)
        app_nana = Application.objects.create(cyd=nana, status=dikembalikan, regulation=reguler,
                                              destination_level=lektor_kepala)
        app_nene = Application.objects.create(cyd=nene, status=diproses_sdm, regulation=reguler,
                                              destination_level=lektor_kepala)

        self.waktu_diproses = StatusChangeRecord.objects.create(cyd=sukarwo, status=diproses_sdm,
                                                                description='Diproses SDM',
                                                                created_at='2017-03-21 07:43:02').created_at
        self.waktu_dikembalikan_sdm = StatusChangeRecord.objects.create(cyd=sukarwo, status=dikembalikan,
                                                                        description='Acceptance letter bermasalah',
                                                                        created_at='2017-03-22 07:43:02').created_at
        revisi = StatusChangeRecord.objects.create(cyd=sukarwo, status=dikembalikan_dgb, description='Catatan: ',
                                                   created_at='2017-03-23 07:43:02')
        self.waktu_dikembalikan_dgb = revisi.created_at
        StatusChangeRecord.objects.create(cyd=ahmad, status=diproses_dgb, description='Diproses DGB')
        self.waktu_terima = StatusChangeRecord.objects.create(cyd=ahmad, status=disetujui,
                                                              description='Pengajuan disetujui').created_at
        StatusChangeRecord.objects.create(cyd=susan, status=diproses_sdm, description='Diproses SDM')
        self.waktu_tolak = StatusChangeRecord.objects.create(cyd=susan, status=dibatalkan,
                                                             description='Pengajuan dibatalkan atas permintaan CYD').created_at

        app_hist_sukarwo = ApplicationHistory.objects.create(cyd=sukarwo, description="Perlu revisi", revision_number=1,
                                                             change_record=revisi)

        regulationQuestion = RegulationQuestion.objects.create(creator=admin, regulation=reguler,
                                                               question="Pernah mengikuti kegiatan X")

        record = Record.objects.create(application_history=app_hist_sukarwo, regulation_question=regulationQuestion,
                                       record="Sertifikat belum dilampirkan")

        self.berkas = DocumentBundle.objects.create(application=app_ahmad, document='berkas/2017/03/22/Artikel.zip')
        self.acc = AcceptanceLetter.objects.create(application=app_ahmad,
                                                   document='acc_letter/2017/03/22/bajaj2015.pdf')
        AcceptanceLetter.objects.create(application=app_sukarwo, document='berkas/2017/03/22/Artikel.zip')
        AcceptanceLetter.objects.create(application=app_sumarni, document='acc_letter/2017/03/22/bajaj2015.pdf')

    def setUp(self):
        self.client = Client()
        self.factory = RequestFactory()
        self.create_objects()

    def test_fakultas_pantau_aktif_view(self):
        url = reverse("fakultas_pantau_aktif")
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'app/fakultas/fakultas_pantau_aktif.html')

    def test_fakultas_pantau_disetujui_view(self):
        url = reverse("fakultas_pantau_disetujui")
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'app/fakultas/fakultas_pantau_disetujui.html')

    def test_fakultas_pantau_dibatalkan_view(self):
        url = reverse("fakultas_pantau_dibatalkan")
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'app/fakultas/fakultas_pantau_dibatalkan.html')

    def test_download_view(self):
        resp_berkas_expect = HttpResponse('resp1')
        resp_acc_expect = HttpResponse('resp2')
        with patch('app.controllers.download_file.download_berkas',
                   return_value=resp_berkas_expect), patch(
                'app.controllers.download_file.download_acc', return_value=resp_acc_expect):
            resp_berkas = self.client.get(reverse('download_berkas', kwargs={'cyd_id': self.ahmad.id}))
            resp_acc = self.client.get(reverse('download_acc', kwargs={'cyd_id': self.ahmad.id}))
            self.assertEqual(resp_berkas.content, resp_berkas_expect.content)
            self.assertEqual(resp_acc.content, resp_acc_expect.content)

    def test_cyd_dikembalikan(self):
        cyd = fmac.get_cyd(self.teknik.id, 'Dikembalikan')
        for c in cyd:
            self.assertEqual(c.faculty.name, 'Teknik')
            app = Application.objects.get(cyd=c.id)
            self.assertEqual(app.status.name.split()[0], 'Dikembalikan')

    def test_cyd_diproses(self):
        cyd1 = fmac.get_cyd(self.teknik.id, 'Diproses')
        cyd2 = fmac.get_cyd(self.teknik.id, 'Diajukan')
        for c in cyd1:
            self.assertEqual(c.faculty.name, 'Teknik')
            app = Application.objects.get(cyd=c.id)
            self.assertEqual(app.status.name.split()[0], 'Diproses')
        for c in cyd2:
            self.assertEqual(c.faculty.name, 'Teknik')
            app = Application.objects.get(cyd=c.id)
            self.assertEqual(app.status.name.split()[0], 'Diajukan')

    def test_cyd_dibatalkan(self):
        cyd = fmac.get_cyd(self.teknik.id, 'Dibatalkan')
        for c in cyd:
            self.assertEqual(c.faculty.name, 'Teknik')
            app = Application.objects.get(cyd=c.id)
            self.assertEqual(app.status.name.split()[0], 'Dibatalkan')

    def test_cyd_disetujui(self):
        cyd = fmac.get_cyd(self.teknik.id, 'Disetujui')
        for c in cyd:
            self.assertEqual(c.faculty.name, 'Teknik')
            app = Application.objects.get(cyd=c.id)
            self.assertEqual(app.status.name.split()[0], 'Disetujui')

    def test_tabel_dibatalkan(self):
        dibatalkan = fmac.get_cyd(self.teknik.id, 'Dibatalkan')
        dibatalkan_list = fmac.cyd_dibatalkan_list(dibatalkan)
        for cyd in dibatalkan_list:
            self.assertEqual(cyd['name'], 'Susan')
            self.assertEqual(cyd['nip'], '111114')
            self.assertEqual(cyd['level'], 'Lektor')
            self.assertEqual(cyd['dest_level'], 'Lektor Kepala')
            self.assertEqual(cyd['time'], self.waktu_tolak.date())

    def test_tabel_disetujui(self):
        disetujui = fmac.get_cyd(self.teknik.id, 'Disetujui')
        disetujui_list = fmac.cyd_disetujui_list(disetujui)
        for cyd in disetujui_list:
            self.assertEqual(cyd['name'], 'Ahmad')
            self.assertEqual(cyd['nip'], '111113')
            self.assertEqual(cyd['level'], 'Lektor')
            self.assertEqual(cyd['dest_level'], 'Lektor Kepala')
            self.assertEqual(cyd['time'], self.waktu_terima.date())

    def test_tabel_diproses(self):
        diproses = fmac.get_cyd(self.teknik.id, 'Diproses')
        diproses_list = fmac.cyd_diproses_list(diproses)
        cyd = diproses_list[1]
        self.assertEqual(cyd['name'], 'Sumarni')
        self.assertEqual(cyd['nip'], '111111')
        self.assertEqual(cyd['level'], 'Lektor')
        self.assertEqual(cyd['dest_level'], 'Lektor Kepala')
        self.assertEqual(cyd['status'], 'Diproses SDM')
        cyd = diproses_list[0]
        self.assertEqual(cyd['name'], 'Nene')
        self.assertEqual(cyd['nip'], '111116')
        self.assertEqual(cyd['level'], 'Lektor')
        self.assertEqual(cyd['dest_level'], 'Lektor Kepala')
        self.assertEqual(cyd['status'], 'Diproses SDM')
        diproses = fmac.get_cyd(self.teknik.id, 'Diajukan')
        diproses_list = fmac.cyd_diproses_list(diproses)
        cyd = diproses_list[0]
        self.assertEqual(cyd['name'], 'Jarot')
        self.assertEqual(cyd['nip'], '111117')
        self.assertEqual(cyd['level'], 'Lektor')
        self.assertEqual(cyd['dest_level'], 'Lektor Kepala')
        self.assertEqual(cyd['status'], 'Diajukan Fakultas')

    def test_tabel_dikembalikan(self):
        dikembalikan = fmac.get_cyd(self.teknik.id, 'Dikembalikan')
        dikembalikan_list = fmac.cyd_dikembalikan_list(dikembalikan)
        cyd = dikembalikan_list[1]
        self.assertEqual(cyd['name'], 'Sukarwo')
        self.assertEqual(cyd['nip'], '111112')
        self.assertEqual(cyd['level'], 'Lektor')
        self.assertEqual(cyd['dest_level'], 'Lektor Kepala')
        self.logtest(cyd=cyd)
        cyd = dikembalikan_list[0]
        self.assertEqual(cyd['name'], 'Nana')
        self.assertEqual(cyd['nip'], '111115')
        self.assertEqual(cyd['level'], 'Lektor')
        self.assertEqual(cyd['dest_level'], 'Lektor Kepala')

    def logtest(self, cyd):
        logs = cyd['log']
        log1 = logs[0]
        self.assertEqual(log1['title'], '1. Diproses SDM pada ' + '2017-03-21')
        log2 = logs[1]
        self.assertEqual(log2['title'], '2. Dikembalikan SDM pada ' + '2017-03-22')
        self.assertEqual(log2['detail'], 'Acceptance letter bermasalah')
        log3 = logs[2]
        self.assertEqual(log3['title'], '3. Dikembalikan DGB pada ' + '2017-03-23')
        self.assertEqual(log3['detail'], 'Catatan: ')
        notes = log3['notes']
        for n in notes:
            self.assertEqual(n.regulation_question.question, 'Pernah mengikuti kegiatan X')
            self.assertEqual(n.record, 'Sertifikat belum dilampirkan')

    def test_exception(self):
        with patch('app.controllers.fakultas_memantau_aplikasi_cyd.get_status_name', side_effect=ObjectDoesNotExist):
            with self.assertRaises(fmac.StatusNotFound):
                fmac.get_status_list([self.ahmad])
            self.assertEqual(fmac.cyd_diproses_list([self.ahmad]), [])
        with patch('app.controllers.fakultas_memantau_aplikasi_cyd.get_level_name', side_effect=ObjectDoesNotExist):
            with self.assertRaises(fmac.JabatanNotFound):
                fmac.get_jabatan_dituju_list([self.ahmad])
            self.assertEqual(fmac.cyd_dikembalikan_list([self.ahmad]), [])
        with patch('app.controllers.fakultas_memantau_aplikasi_cyd.get_waktu', side_effect=ObjectDoesNotExist):
            with self.assertRaises(fmac.WaktuNotFound):
                fmac.get_waktu_list([self.ahmad], 'Disetujui')
            self.assertEqual(fmac.cyd_disetujui_list([self.ahmad]), [])
        with patch('app.controllers.fakultas_memantau_aplikasi_cyd.get_app_history', side_effect=ObjectDoesNotExist):
            with self.assertRaises(fmac.LogNotFound):
                fmac.get_log_list([self.sukarwo])
            self.assertEqual(fmac.cyd_dibatalkan_list([self.ahmad]), [])

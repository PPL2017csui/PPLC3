from django.test import TestCase
from django.urls import reverse
from django.test import Client
from mock import patch
from django.core.exceptions import ObjectDoesNotExist
from ..models import Role, Admin, Level, Faculty, Status, Regulation, RegulationQuestion, CalonYangDiusulkan, \
    Application, ApplicationHistory, Record, StatusChangeRecord, DocumentBundle, AcceptanceLetter
from ..controllers import dgb_melihat_riwayat_pengajuan as dmrp
from ..controllers import download_file as df
from mock import MagicMock


class ApplicationHistoryTest(TestCase):
    def create_objects(self):
        role = Role.objects.create(name="DGB")
        regulation = Regulation.objects.create(name='Reguler 001')
        teknik = Faculty.objects.create(name='Fakultas Teknik')
        diterima = Status.objects.create(name='Disetujui', id=1)
        dibatalkan = Status.objects.create(name='Ditolak', id=3)
        lektor = Level.objects.create(name='Lektor')
        lektor_kepala = Level.objects.create(name='Lektor Kepala')
        admin = Admin.objects.create(nip=1000067891234567, role=role)
        regulationQuestion = RegulationQuestion.objects.create(creator=admin, regulation=regulation,
                                                               question="apakah ijazah asli?")
        self.ahmad = CalonYangDiusulkan.objects.create(nip='111113', name='Ahmad', birthplace='Malang',
                                                       current_level=lektor,
                                                       email='ahmad@abc.com', faculty=teknik, gender='L')
        susan = CalonYangDiusulkan.objects.create(nip='111114', name='Susan', birthplace='Depok', current_level=lektor,
                                                  email='susan@abc.com', faculty=teknik, gender='P')

        app_ahmad = Application.objects.create(cyd=self.ahmad, status=diterima, regulation=regulation,
                                               destination_level=lektor_kepala)
        app_susan = Application.objects.create(cyd=susan, status=dibatalkan, regulation=regulation,
                                               destination_level=lektor_kepala)

        change_record_diterima = StatusChangeRecord.objects.create(cyd=self.ahmad, status=diterima,
                                                                   description="diterima")
        change_record_ditolak = StatusChangeRecord.objects.create(cyd=susan, status=dibatalkan, description="ditolak")

        app1 = ApplicationHistory.objects.create(cyd=self.ahmad, description="diterima", revision_number=1,
                                                 change_record=change_record_diterima)
        app2 = ApplicationHistory.objects.create(cyd=susan, description="ditolak", revision_number=1,
                                                 change_record=change_record_ditolak)
        record1 = Record.objects.create(application_history=app1, regulation_question=regulationQuestion,
                                        record="Ijazah ada legalisir")
        record2 = Record.objects.create(application_history=app2, regulation_question=regulationQuestion,
                                        record="Ijazah tidak ada legalisir")
        self.berkas = DocumentBundle.objects.create(application=app_ahmad, document='berkas/2017/03/22/Artikel.zip')
        self.acc = AcceptanceLetter.objects.create(application=app_ahmad,
                                                   document='acc_letter/2017/03/22/bajaj2015.pdf')

    def setUp(self):
        self.create_objects()
        self.client = Client()
        self.session = self.client.session

    def test_dmrp_get_accepted_application(self):
        accepted_applications = dmrp.get_accepted_applications()
        self.assertGreater(len(accepted_applications), 0)
        for acc_app in accepted_applications:
            self.assertEqual(acc_app['name'], 'Ahmad')
            self.assertEqual(acc_app['nip'], '111113')
            self.assertEqual(acc_app['faculty'], 'Fakultas Teknik')
            self.assertEqual(acc_app['current_level'], 'Lektor')
            self.assertEqual(acc_app['destination_level'], 'Lektor Kepala')
            app_history = ApplicationHistory.objects.get(description="diterima")
            notes = Record.objects.filter(application_history=app_history.id)
            note_str_1 = ''
            note_str_2 = ''
            logs = acc_app['logs']
            for log in logs:
                rev_number = log['revision_number']
                note_str_1 += str(rev_number)
                for note in log['notes']:
                    note_str_1 += str(note)

        note_str_2 += str(app_history.revision_number)
        for note in notes:
            note_str_2 += str(note)

        self.assertEqual(note_str_1, note_str_2)

    def test_dmrp_get_rejected_application(self):
        accepted_applications = dmrp.get_rejected_applications()
        self.assertGreater(len(accepted_applications), 0)
        for acc_app in accepted_applications:
            self.assertEqual(acc_app['name'], 'Susan')
            self.assertEqual(acc_app['nip'], '111114')
            self.assertEqual(acc_app['faculty'], 'Fakultas Teknik')
            self.assertEqual(acc_app['current_level'], 'Lektor')
            self.assertEqual(acc_app['destination_level'], 'Lektor Kepala')
            app_history = ApplicationHistory.objects.get(description="ditolak")
            notes = Record.objects.filter(record='Ijazah tidak ada legalisir')
            note_str_1 = '';
            note_str_2 = '';
            logs = acc_app['logs']
            for log in logs:
                rev_number = log['revision_number']
                note_str_1 += str(rev_number)
                for note in log['notes']:
                    note_str_1 += str(note)

            note_str_2 += str(app_history.revision_number)
            for note in notes:
                note_str_2 += str(note)
            self.assertEqual(note_str_1, note_str_2)

    def test_applications_history(self):
        url = reverse("dgb_application_history")
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

    def test_exception(self):
        with patch('app.models.Application.objects.get', side_effect=ObjectDoesNotExist):
            self.assertEqual(dmrp.get_accepted_applications(), [])
        with patch('app.models.Application.objects.get', side_effect=ObjectDoesNotExist):
            self.assertEqual(dmrp.get_rejected_applications(), [])

    def test_download_success(self):
        resp_berkas = df.download_berkas(self.ahmad.id)
        resp_acc = df.download_acc(self.ahmad.id)
        berkas = open(self.berkas.document.path, 'rb').read()
        acc = open(self.acc.document.path, 'rb').read()
        self.assertEqual(resp_berkas.content, berkas)
        self.assertEqual(resp_acc.content, acc)

    def test_download_false_path(self):
        fake_doc = MagicMock()
        fake_doc.document.path = 'randomstring-askdasdasdkk'
        with patch('os.path.exists', return_value=False):
            resp_berkas = df.download_berkas(self.ahmad.id)
            resp_acc = df.download_acc(self.ahmad.id)
            self.assertEqual(resp_berkas.content, b'<html><h3>Berkas tidak ditemukan</h3></html>')
            self.assertEqual(resp_acc.content, b'<html><h3>Acceptance letter belum tersedia</h3></html>')

    def test_download_document_not_exist(self):
        with patch('app.models.DocumentBundle.objects.get', side_effect=ObjectDoesNotExist), patch(
                'app.models.AcceptanceLetter.objects.get', side_effect=ObjectDoesNotExist):
            resp_berkas = df.download_berkas(self.ahmad.id)
            resp_acc = df.download_acc(self.ahmad.id)
            self.assertEqual(resp_berkas.content, b'<html><h3>Berkas tidak ditemukan</h3></html>')
            self.assertEqual(resp_acc.content, b'<html><h3>Acceptance letter belum tersedia</h3></html>')

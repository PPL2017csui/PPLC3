from django.test import TestCase
from django.urls import reverse
from ..controllers import fakultas_ajukan_berkas as fab
from ..models import CalonYangDiusulkan, Role, Admin, Level, Faculty, Status, Regulation, RegulationQuestion, \
	Application
from django.core.files.uploadedfile import SimpleUploadedFile


class FakultasAjukanBerkasTest(TestCase):
	def test_fakultas_ajukan_berkas_view(self):

		status = Status.objects.create(name="Diajukan Fakultas")
		role = Role.objects.create(name="Fakultas")
		admin = Admin.objects.create(nip=123456789123456789, role=role)
		level1 = Level.objects.create(name="Staff Ahli")
		level2 = Level.objects.create(name="Lektor")
		level3 = Level.objects.create(name="Lektor Kepala")
		level4 = Level.objects.create(name="Guru Besar")

		faculty = Faculty.objects.create(name="Fakultas Ilmu Komputer")
		regulation = Regulation.objects.create(name="Aturan 001")
		a = Admin.objects.get(nip=123456789123456789)
		regulationQuestion = RegulationQuestion.objects.create(creator=a, regulation=regulation,
															   question="apakah ijazah asli?")
		cyd1 = CalonYangDiusulkan.objects.create(nip=123456789123456789, faculty=faculty, current_level=level1,
												 name="Sutopo1")
		cyd2 = CalonYangDiusulkan.objects.create(nip=123456789123456780, faculty=faculty, current_level=level2,
												 name="Sutopo2")
		cyd3 = CalonYangDiusulkan.objects.create(nip=123456789123456781, faculty=faculty, current_level=level3,
												 name="Sutopo3")
		cyd4 = CalonYangDiusulkan.objects.create(nip=123456789123456782, faculty=faculty, current_level=level4,
												 name="Sutopo4")

		application = Application.objects.create(cyd=cyd1, status=status, regulation=regulation,
												 destination_level=level2)

		doc = SimpleUploadedFile("test.txt", b"file_content")

		response = self.client.post(reverse('fakultas_upload'), {"NIP": "-", "file": doc, "recc_type": 1})

		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'app/fakultas/upload.html')

		response = self.client.post(reverse('fakultas_upload'),
									{"NIP": 123456789123456789, "file": doc, "recc_type": 1})

		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'app/fakultas/upload.html')

		response = self.client.post(reverse('fakultas_upload'),
									{"NIP": 123456789123456780, "file": doc, "recc_type": 3})

		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'app/fakultas/upload.html')

		response = self.client.post(reverse('fakultas_upload'),
									{"NIP": 123456789123456780, "file": doc, "recc_type": 1})

		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'app/fakultas/upload.html')

		response = self.client.get(reverse('fakultas_upload'))

		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'app/fakultas/upload.html')

	def test_upload(self):

		Status.objects.create(name="Diajukan Fakultas")

		role = Role.objects.create(name="Fakultas")
		admin = Admin.objects.create(nip=123456789123456789, role=role)
		level1 = Level.objects.create(name="Staff Ahli")
		level2 = Level.objects.create(name="Lektor")
		level3 = Level.objects.create(name="Lektor Kepala")
		level4 = Level.objects.create(name="Guru Besar")

		faculty = Faculty.objects.create(name="Fakultas Ilmu Komputer")
		regulation = Regulation.objects.create(name="Aturan 001")
		a = Admin.objects.get(nip=123456789123456789)
		regulationQuestion = RegulationQuestion.objects.create(creator=a, regulation=regulation,
															   question="apakah ijazah asli?")
		cyd1 = CalonYangDiusulkan.objects.create(nip=123456789123456789, faculty=faculty, current_level=level1,
												 name="Sutopo1")
		cyd2 = CalonYangDiusulkan.objects.create(nip=123456789123456780, faculty=faculty, current_level=level2,
												 name="Sutopo2")
		cyd3 = CalonYangDiusulkan.objects.create(nip=123456789123456781, faculty=faculty, current_level=level3,
												 name="Sutopo3")
		cyd4 = CalonYangDiusulkan.objects.create(nip=123456789123456782, faculty=faculty, current_level=level4,
												 name="Sutopo4")

		fab.fakultas_upload(nip="123456789123456789", file=None, recc_type=1)
		fab.fakultas_upload(nip="123456789123456789", file=None, recc_type=2)
		try:
			fab.fakultas_upload(nip="123456789123456789", file=None, recc_type=3)
		except fab.LevelExceededException:
			pass

		fab.fakultas_upload(nip="123456789123456780", file=None, recc_type=1)
		fab.fakultas_upload(nip="123456789123456780", file=None, recc_type=2)
		try:
			fab.fakultas_upload(nip="123456789123456780", file=None, recc_type=3)
		except fab.LevelExceededException:
			pass

		fab.fakultas_upload(nip="123456789123456781", file=None, recc_type=1)
		try:
			fab.fakultas_upload(nip="123456789123456781", file=None, recc_type=2)
		except fab.LevelExceededException:
			pass

		try:
			fab.fakultas_upload(nip="123456789123456782", file=None, recc_type=2)
		except fab.LevelExceededException:
			pass

		# test dengan NIP 123456789123456789 yang telah dibuat
		self.assertEqual(Application.objects.filter(cyd=cyd1).count(), 2)
		self.assertEqual(Application.objects.filter(cyd=cyd2).count(), 2)
		self.assertEqual(Application.objects.filter(cyd=cyd3).count(), 1)
		self.assertEqual(Application.objects.filter(cyd=cyd4).count(), 0)

	def test_fakultas_ajukan_kembali_view(self) :
		status = Status.objects.create(name="Dikembalikan SDM")
		status2 = Status.objects.create(name="Dikembalikan DGB")
		status3 = Status.objects.create(name="Diajukan Fakultas")
		newstatus = Status.objects.create(name="Diajukan Fakultas")
		role = Role.objects.create(name="Fakultas")
		admin = Admin.objects.create(nip=123456789123456789, role=role)
		level1 = Level.objects.create(name="Staff Ahli")
		level2 = Level.objects.create(name="Lektor")
		level3 = Level.objects.create(name="Lektor Kepala")
		level4 = Level.objects.create(name="Guru Besar")

		faculty = Faculty.objects.create(name="Fakultas Ilmu Komputer")
		regulation = Regulation.objects.create(name="Aturan 001")
		a = Admin.objects.get(nip=123456789123456789)
		regulationQuestion = RegulationQuestion.objects.create(creator=a, regulation=regulation, question="apakah ijazah asli?")
		cyd1 = CalonYangDiusulkan.objects.create(nip=123456789123456789, faculty=faculty, current_level=level1, name="Sutopo1")
		cyd2 = CalonYangDiusulkan.objects.create(nip=123456789123456780, faculty=faculty, current_level=level2, name="Sutopo2")
		cyd3 = CalonYangDiusulkan.objects.create(nip=123456789123456781, faculty=faculty, current_level=level1, name="Sutopo3")

		Application.objects.create(cyd=cyd1, status=status, regulation=regulation, destination_level=level2)
		Application.objects.create(cyd=cyd2, status=status2, regulation=regulation, destination_level=level2)
		Application.objects.create(cyd=cyd3, status=status3, regulation=regulation, destination_level=level2)


		doc = SimpleUploadedFile("test.txt", b"file_content")

		response = self.client.get(reverse('fakultas_upload_revision'))

		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'app/fakultas/fakultas_ajukan_kembali.html')

		response = self.client.post(reverse('fakultas_upload_revision'), {"NIP": "-", "file": doc})

		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'app/fakultas/fakultas_ajukan_kembali.html')

		response = self.client.post(reverse('fakultas_upload_revision'), {"NIP": 123456789123456781, "file": doc})

		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'app/fakultas/fakultas_ajukan_kembali.html')

		response = self.client.post(reverse('fakultas_upload_revision'), {"NIP": 123456789123456780, "file": doc})

		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'app/fakultas/fakultas_ajukan_kembali.html')

		response = self.client.post(reverse('fakultas_upload_revision'), {"NIP": 123456789123456780, "file": doc, "confirmation" : "yes"})

		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'app/fakultas/fakultas_ajukan_kembali.html')

	def test_fakultas_ajukan_kembali(self):

		status = Status.objects.create(name="Dikembalikan SDM")
		status2 = Status.objects.create(name="Dikembalikan DGB")
		newstatus = Status.objects.create(name="Diajukan Fakultas")
		role = Role.objects.create(name="Fakultas")
		admin = Admin.objects.create(nip=123456789123456789, role=role)
		level1 = Level.objects.create(name="Staff Ahli")
		level2 = Level.objects.create(name="Lektor")
		level3 = Level.objects.create(name="Lektor Kepala")
		level4 = Level.objects.create(name="Guru Besar")

		faculty = Faculty.objects.create(name="Fakultas Ilmu Komputer")
		regulation = Regulation.objects.create(name="Aturan 001")
		a = Admin.objects.get(nip=123456789123456789)
		regulationQuestion = RegulationQuestion.objects.create(creator=a, regulation=regulation,
															   question="apakah ijazah asli?")
		cyd1 = CalonYangDiusulkan.objects.create(nip=123456789123456789, faculty=faculty, current_level=level1,
												 name="Sutopo1")
		cyd2 = CalonYangDiusulkan.objects.create(nip=123456789123456780, faculty=faculty, current_level=level2,
												 name="Sutopo2")

		Application.objects.create(cyd=cyd1, status=status, regulation=regulation, destination_level=level2)
		Application.objects.create(cyd=cyd2, status=status2, regulation=regulation, destination_level=level2)

		fab.fakultas_ajukan_kembali(nip=123456789123456789, file=None)

		fab.fakultas_ajukan_kembali(nip=123456789123456780, file=None)

		self.assertEquals(str(Application.objects.get(cyd=cyd1).status), str(newstatus.id) + " " + newstatus.name)

		self.assertEquals(str(Application.objects.get(cyd=cyd2).status), str(newstatus.id) + " " + newstatus.name)

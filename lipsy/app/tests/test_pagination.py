from unittest import TestCase
from django.test import Client
from django.urls import reverse


class PaginationTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_pagination(self):
        session = self.client.session
        session['page3'] = [10]
        session['page2'] = ['not number']
        session['page1'] = [10000000000]
        session.save()
        url1 = reverse("fakultas_pantau_aktif")
        url2 = reverse("fakultas_pantau_disetujui")
        url3 = reverse("fakultas_pantau_dibatalkan")
        self.client.get(url1)
        self.client.get(url2)
        self.client.get(url3)

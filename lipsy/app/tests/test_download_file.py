from django.test import TestCase
from django.test import Client
from mock import patch
from django.core.exceptions import ObjectDoesNotExist
from ..models import Level, Faculty, Status, Regulation, CalonYangDiusulkan, Application, DocumentBundle, \
    AcceptanceLetter
from ..controllers import download_file as df
from mock import MagicMock


class DownloadFileTest(TestCase):
    def create_objects(self):
        regulation = Regulation.objects.create(name='Reguler 001')
        teknik = Faculty.objects.create(name='Fakultas Teknik')
        diterima = Status.objects.create(name='Disetujui', id=1)
        lektor = Level.objects.create(name='Lektor')
        lektor_kepala = Level.objects.create(name='Lektor Kepala')
        self.ahmad = CalonYangDiusulkan.objects.create(nip='111113', name='Ahmad', birthplace='Malang',
                                                       current_level=lektor,
                                                       email='ahmad@abc.com', faculty=teknik, gender='L')
        app_ahmad = Application.objects.create(cyd=self.ahmad, status=diterima, regulation=regulation,
                                               destination_level=lektor_kepala)
        self.berkas = DocumentBundle.objects.create(application=app_ahmad, document='berkas/2017/03/22/Artikel.zip')
        self.acc = AcceptanceLetter.objects.create(application=app_ahmad,
                                                   document='acc_letter/2017/03/22/bajaj2015.pdf')

    def setUp(self):
        self.create_objects()
        self.client = Client()
        self.session = self.client.session

    def test_download_success(self):
        resp_berkas = df.download_berkas(self.ahmad.id)
        resp_acc = df.download_acc(self.ahmad.id)
        berkas = open(self.berkas.document.path, 'rb').read()
        acc = open(self.acc.document.path, 'rb').read()
        self.assertEqual(resp_berkas.content, berkas)
        self.assertEqual(resp_acc.content, acc)

    def test_download_false_path(self):
        fake_doc = MagicMock()
        fake_doc.document.path = 'randomstring-askdasdasdkk'
        with patch('os.path.exists', return_value=False):
            resp_berkas = df.download_berkas(self.ahmad.id)
            resp_acc = df.download_acc(self.ahmad.id)
            self.assertEqual(resp_berkas.content, b'<html><h3>Berkas tidak ditemukan</h3></html>')
            self.assertEqual(resp_acc.content, b'<html><h3>Acceptance letter belum tersedia</h3></html>')

    def test_download_document_not_exist(self):
        with patch('app.models.DocumentBundle.objects.get', side_effect=ObjectDoesNotExist), patch(
                'app.models.AcceptanceLetter.objects.get', side_effect=ObjectDoesNotExist):
            resp_berkas = df.download_berkas(self.ahmad.id)
            resp_acc = df.download_acc(self.ahmad.id)
            self.assertEqual(resp_berkas.content, b'<html><h3>Berkas tidak ditemukan</h3></html>')
            self.assertEqual(resp_acc.content, b'<html><h3>Acceptance letter belum tersedia</h3></html>')

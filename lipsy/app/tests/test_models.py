from django.test import TestCase

from ..models import Role, Admin, Level, Faculty, Status, Regulation, RegulationQuestion, CalonYangDiusulkan, \
	Application, EducationHistory, ApplicationHistory, Record, StatusChangeRecord, DocumentBundle, AcceptanceLetter


class ModelTest(TestCase):
	def create_object(self):
		role = Role.objects.create(name="Fakultas")
		admin = Admin.objects.create(nip=123456789123456789, role=role)
		level = Level.objects.create(name="Lektor kepala")
		faculty = Faculty.objects.create(name="Fakultas Ilmu Komputer")
		status = Status.objects.create(name="Disetujui")
		regulation = Regulation.objects.create(name="Aturan 001")
		a = Admin.objects.get(nip=123456789123456789)
		regulationQuestion = RegulationQuestion.objects.create(creator=a, regulation=regulation,
															   question="apakah ijazah asli?")
		cyd = CalonYangDiusulkan.objects.create(nip=123456789123456789, faculty=faculty, current_level=level,
												name="Sutopo")
		application = Application.objects.create(status=status, regulation=regulation, destination_level=level,
												 cyd=cyd)
		educationHistory = EducationHistory.objects.create(cyd=cyd, order=1, name="SD Harapan Bangsa")
		appHistory = ApplicationHistory.objects.create(cyd=cyd, revision_number=1,
													   description="Tidak mendapat persetujuan rektor")
		record = Record.objects.create(application_history=appHistory, regulation_question=regulationQuestion,
									   record="Ijazah tidak ada legalisir")
		statusChangeRecord = StatusChangeRecord.objects.create(cyd=cyd, status=status,
															   description="Ijazah tidak ada legalisir")

	def setUp(self):
		self.create_object()

	def test_role(self):
		role = Role.objects.get(name="Fakultas")
		self.assertEqual(str(role), (str(role.id) + " " + role.name))

	def test_admin(self):
		admin = Admin.objects.get(nip=123456789123456789)
		self.assertEquals(str(admin), str(admin.nip))

	def test_level(self):
		level = Level.objects.get(name="Lektor kepala")
		self.assertEquals(str(level), str(level.id) + " " + level.name)

	def test_faculty(self):
		faculty = Faculty.objects.get(name="Fakultas Ilmu Komputer")
		self.assertEquals(str(faculty), str(faculty.id) + " " + faculty.name)

	def test_status(self):
		status = Status.objects.get(name="Disetujui")
		self.assertEquals(str(status), str(status.id) + " " + status.name)

	def test_regulation(self):
		regulation = Regulation.objects.get(name="Aturan 001")
		self.assertEquals(str(regulation), str(regulation.id) + " " + regulation.name)

	def test_regulation_question(self):
		regulationQuestion = RegulationQuestion.objects.get(question="apakah ijazah asli?")
		self.assertEquals(str(regulationQuestion), str(regulationQuestion.id) + " " + regulationQuestion.question)

	def test_cyd(self):
		cyd = CalonYangDiusulkan.objects.get(nip=123456789123456789)
		self.assertEquals(str(cyd), str(cyd.id) + " " + cyd.name)

	def test_application(self):
		cyd = CalonYangDiusulkan.objects.get(nip=123456789123456789)
		application = Application.objects.get(cyd=cyd)
		self.assertEquals(str(application), str(application.id) + " " + str(application.cyd.nip))

	def test_education_history(self):
		educationHistory = EducationHistory.objects.get(name="SD Harapan Bangsa")
		self.assertEquals(str(educationHistory), str(educationHistory.id) + " " + educationHistory.name)

	def test_application_history(self):
		appHistory = ApplicationHistory.objects.get(description="Tidak mendapat persetujuan rektor")
		self.assertEquals(str(appHistory), str(appHistory.id) + " " + appHistory.description)

	def test_record(self):
		record = Record.objects.get(record="Ijazah tidak ada legalisir")
		self.assertEquals(str(record), str(record.id) + " " + record.record)

	def test_status_change_record(self):
		statusChangeRecord = StatusChangeRecord.objects.get(description="Ijazah tidak ada legalisir")
		self.assertEquals(str(statusChangeRecord), str(statusChangeRecord.id) + " " + statusChangeRecord.description)
	
	def test_application(self):
		role = Role.objects.get(name="Fakultas")
		level = Level.objects.get(name="Lektor kepala")
		faculty = Faculty.objects.get(name="Fakultas Ilmu Komputer")
		status = Status.objects.get(name="Disetujui")
		regulation = Regulation.objects.get(name="Aturan 001")
		a = Admin.objects.get(nip=123456789123456789)
		regulationQuestion = RegulationQuestion.objects.get(question="apakah ijazah asli?")
		cyd = CalonYangDiusulkan.objects.get(nip=123456789123456789)

		application = Application.objects.create(cyd=cyd, status=status, regulation=regulation, destination_level=level)
		document = DocumentBundle.objects.create(application=application, document='berkas/2017/03/23/test_YxEcOV9.txt')
		acc_letter = AcceptanceLetter.objects.create(application=application, document='acc_letter/2017/03/23/test.txt')

		self.assertEquals(str(application), "{0} {1}".format(application.id, application.cyd.nip))
		self.assertEquals(str(document), str(document.id) + ' ' + 'berkas/2017/03/23/test_YxEcOV9.txt')
		self.assertEquals(str(acc_letter), str(acc_letter.id) + ' ' + 'acc_letter/2017/03/23/test.txt')

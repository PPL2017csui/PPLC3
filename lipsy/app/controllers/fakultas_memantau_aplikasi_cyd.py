from ..models import CalonYangDiusulkan, Application, Status, Level, StatusChangeRecord, ApplicationHistory, \
    Record, AcceptanceLetter
from django.core.exceptions import ObjectDoesNotExist


class StatusNotFound(Exception):
    pass


class JabatanNotFound(Exception):
    pass


class WaktuNotFound(Exception):
    pass


class LogNotFound(Exception):
    pass


def get_cyd(fakultas, status):
    status_arr = Status.objects.filter(name__contains=status).values_list('id', flat=True)
    nips = Application.objects.filter(status__in=status_arr).values_list('cyd', flat=True)
    cyd = CalonYangDiusulkan.objects.filter(faculty=fakultas, id__in=nips).order_by('name')
    return cyd


def get_status_name(cyd_id):
    status_id = Application.objects.get(cyd=cyd_id).status.id
    status_name = Status.objects.get(id=status_id).name
    return status_name


def get_status_list(cyds):
    try:
        status = []
        for cyd in cyds:
            status_name = get_status_name(cyd.id)
            status += [status_name]
        return status
    except ObjectDoesNotExist:
        raise StatusNotFound()


def get_level_name(cyd_id):
    level_id = Application.objects.get(cyd=cyd_id).destination_level.id
    level_name = Level.objects.get(id=level_id).name
    return level_name


def get_jabatan_dituju_list(cyds):
    try:
        level = []
        for cyd in cyds:
            level_name = get_level_name(cyd.id)
            level += [level_name]
        return level
    except ObjectDoesNotExist:
        raise JabatanNotFound()


def get_waktu(cyd_id, status):
    status_id = Status.objects.get(name__contains=status).id
    time = StatusChangeRecord.objects.get(cyd=cyd_id, status=status_id).created_at
    return time


def get_waktu_list(cyds, status):
    try:
        times = []
        for cyd in cyds:
            time = get_waktu(cyd.id, status)
            times += [time]
        return times
    except ObjectDoesNotExist:
        raise WaktuNotFound()


def get_app_history(change_id):
    app_his = ApplicationHistory.objects.get(change_record=change_id)
    return app_his


def get_log_list(cyds):
    try:
        cyds_log = []
        for cyd in cyds:
            changes = StatusChangeRecord.objects.filter(cyd=cyd.id).order_by('created_at')
            log = []
            count = 1
            for change in changes:
                log_instance = {}
                if change.status.name == 'Dikembalikan SDM' or change.status.name == 'Dibatalkan':
                    log_instance['title'] = str(count) + ". " + str(change.status.name) + " pada " + str(
                        change.created_at.date())
                    log_instance['detail'] = str(change.description)
                elif change.status.name == 'Dikembalikan DGB':
                    app_history = get_app_history(change.id)
                    notes = Record.objects.filter(application_history=app_history.id)
                    log_instance['title'] = str(count) + ". " + str(change.status.name) + " pada " + str(
                        change.created_at.date())
                    log_instance['detail'] = 'Catatan: '
                    log_instance['notes'] = notes
                else:
                    log_instance['title'] = str(count) + ". " + str(change.status.name) + " pada " + str(
                        change.created_at.date())
                log += [log_instance]
                count += 1
            cyds_log += [log]
        return cyds_log
    except ObjectDoesNotExist:
        raise LogNotFound()


def cyd_diproses_list(diproses):
    try:
        status = get_status_list(diproses)
        jabatan_dituju = get_jabatan_dituju_list(diproses)
        val = list(diproses.values())
        i = 0
        while i < len(val):
            cyd = val[i]
            app_id = Application.objects.get(cyd=cyd['id']).id
            acc = AcceptanceLetter.objects.filter(application=app_id)
            if len(acc) == 0:
                cyd['acc'] = False
            else:
                cyd['acc'] = True
            cyd['status'] = status[i]
            cyd['dest_level'] = jabatan_dituju[i]
            cyd['level'] = diproses[i].current_level.name
            val[i] = cyd
            i += 1
        return val
    except (StatusNotFound, JabatanNotFound):
        return []


def cyd_dikembalikan_list(dikembalikan):
    try:
        jabatan_dituju = get_jabatan_dituju_list(dikembalikan)
        log = get_log_list(dikembalikan)
        val = list(dikembalikan.values())
        i = 0
        while i < len(val):
            cyd = val[i]
            app_id = Application.objects.get(cyd=cyd['id']).id
            acc = AcceptanceLetter.objects.filter(application=app_id)
            if len(acc) == 0:
                cyd['acc'] = False
            else:
                cyd['acc'] = True
            cyd['log'] = log[i]
            cyd['dest_level'] = jabatan_dituju[i]
            cyd['level'] = dikembalikan[i].current_level.name
            val[i] = cyd
            i += 1
        return val
    except (LogNotFound, JabatanNotFound):
        return []


def cyd_dibatalkan_list(dibatalkan):
    try:
        jabatan_dituju = get_jabatan_dituju_list(dibatalkan)
        log = get_log_list(dibatalkan)
        waktu = get_waktu_list(dibatalkan, 'Dibatalkan')
        val = list(dibatalkan.values())
        i = 0
        while i < len(val):
            cyd = val[i]
            cyd['log'] = log[i]
            cyd['dest_level'] = jabatan_dituju[i]
            cyd['level'] = dibatalkan[i].current_level.name
            cyd['time'] = waktu[i].date()
            val[i] = cyd
            i += 1
        return val
    except (LogNotFound, JabatanNotFound, WaktuNotFound):
        return []


def cyd_disetujui_list(disetujui):
    try:
        jabatan_dituju = get_jabatan_dituju_list(disetujui)
        waktu = get_waktu_list(disetujui, 'Disetujui')
        val = list(disetujui.values())
        i = 0
        while i < len(val):
            cyd = val[i]
            cyd['dest_level'] = jabatan_dituju[i]
            cyd['level'] = disetujui[i].current_level.name
            cyd['time'] = waktu[i].date()
            val[i] = cyd
            i += 1
        return val
    except (JabatanNotFound, WaktuNotFound):
        return []

from .. import models
from django.core.exceptions import ObjectDoesNotExist

class ApplicationNotFound(Exception) :
	pass

class LevelExceededException(Exception) :
	pass
	
def fakultas_upload(nip, file, recc_type) :
	CYD = models.CalonYangDiusulkan.objects.get(nip=nip)
	
	regulasi = models.Regulation.objects.order_by('-updated_at').get()
	
	dest_level = None
	if (CYD.current_level.name == "Staff Ahli") :
		if (str(recc_type) == "1") :
			dest_level = models.Level.objects.get(name="Lektor")
		elif (str(recc_type) == "2") :
			dest_level = models.Level.objects.get(name="Lektor Kepala")
		else :
			raise LevelExceededException('Error tidak bisa loncat {0} dari {1}'.format(recc_type, CYD.current_level.name))
	elif (CYD.current_level.name == "Lektor") :
		if (str(recc_type) == "1"):
			dest_level = models.Level.objects.get(name="Lektor Kepala")
		elif (str(recc_type) == "2"):
			dest_level = models.Level.objects.get(name="Guru Besar")
		else :
			raise LevelExceededException('Error tidak bisa loncat {0} dari {1}'.format(recc_type, CYD.current_level.name))
	elif (CYD.current_level.name == "Lektor Kepala") :
		if (str(recc_type) == "1") :
			dest_level = models.Level.objects.get(name="Guru Besar")
		else :
			raise LevelExceededException('Error tidak bisa loncat {0} dari {1}'.format(recc_type, CYD.current_level.name))
	else :
		raise LevelExceededException('{0} tidak bisa melakukan kenaikan jabatan'.format(CYD.current_level.name) )
	
	status 		= models.Status.objects.get(name="Diajukan Fakultas")
	aplikasi 	= models.Application(cyd = CYD, status = status, regulation=regulasi, destination_level=dest_level)
	
	aplikasi.save()
	
	doc = models.DocumentBundle( document = file , application = aplikasi)
	doc.save()
	
def fakultas_ajukan_kembali(nip, file) :
	CYD = models.CalonYangDiusulkan.objects.get(nip = nip)
	newstatus = models.Status.objects.get(name = 'Diajukan Fakultas')
	aplikasi = models.Application.objects.get(cyd = CYD)
	models.Application.objects.filter(cyd = CYD).update(status = newstatus)
	try :
		models.DocumentBundle.objects.filter(application = aplikasi).delete()
	except Exception :
		pass
	models.DocumentBundle.objects.create( document = file, application = aplikasi)
import os

from django.http import HttpResponse

from ..models import Application, StatusChangeRecord, ApplicationHistory, Record, DocumentBundle, AcceptanceLetter
from django.core.exceptions import ObjectDoesNotExist

def get_accepted_applications():
    try:
        accepted_applications = []
        accepted_status_changes = StatusChangeRecord.objects.filter(status=1).order_by('created_at')
        for asc in accepted_status_changes:
            history_instance = {}
            application = Application.objects.get(cyd=asc.cyd.id)
            history_instance['name'] = asc.cyd.name
            history_instance['asc_id'] = asc.id
            history_instance['nip'] = asc.cyd.nip
            history_instance['cyd_id'] = asc.cyd.id
            history_instance['faculty'] = asc.cyd.faculty.name
            history_instance['current_level'] = asc.cyd.current_level.name
            history_instance['destination_level'] = application.destination_level.name
            app_histories = ApplicationHistory.objects.filter(cyd=asc.cyd.id).order_by('revision_number')
            logs = []
            for app_history in app_histories:
                notes = Record.objects.filter(application_history=app_history.id)
                log = {'revision_number': app_history.revision_number, 'notes': notes}
                logs += [log]
            history_instance['logs'] = logs
            accepted_applications += [history_instance]

        return accepted_applications
    except ObjectDoesNotExist:
        return []


def get_rejected_applications():
    try:
        rejected_applications = []
        rejected_status_changes = StatusChangeRecord.objects.filter(status=3).order_by('created_at')
        for rsc in rejected_status_changes:
            history_instance = {}
            application = Application.objects.get(cyd=rsc.cyd.id)
            history_instance['name'] = rsc.cyd.name
            history_instance['nip'] = rsc.cyd.nip
            history_instance['cyd_id'] = rsc.cyd.id
            history_instance['faculty'] = rsc.cyd.faculty.name
            history_instance['current_level'] = rsc.cyd.current_level.name
            history_instance['destination_level'] = application.destination_level.name
            app_histories = ApplicationHistory.objects.filter(cyd=rsc.cyd.id).order_by('revision_number')
            logs = []
            for app_history in app_histories:
                notes = Record.objects.filter(application_history=app_history.id)
                log = {'revision_number': app_history.revision_number, 'notes': notes}
                logs += [log]
            history_instance['logs'] = logs
            rejected_applications += [history_instance]

        return rejected_applications
    except ObjectDoesNotExist:
        return []

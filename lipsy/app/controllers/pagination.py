from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage


def pagination(request, name, input_list):
    if name not in request.session:
        request.session[name] = 1
    page = request.GET.get(name, request.session[name])
    per_page = 10
    paginator = Paginator(input_list, per_page)
    try:
        cyd_list = paginator.page(page)
        request.session[name] = page
    except PageNotAnInteger:
        cyd_list = paginator.page(1)
    except EmptyPage:
        cyd_list = paginator.page(paginator.num_pages)

    return cyd_list

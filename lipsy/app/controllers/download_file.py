import os

from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse

from ..models import Application, AcceptanceLetter, DocumentBundle


def download_berkas(cyd_id):
    try:
        app_id = Application.objects.get(cyd=cyd_id).id
        file_path = DocumentBundle.objects.get(application=app_id).document.path
        if os.path.exists(file_path):
            with open(file_path, 'rb') as fh:
                response = HttpResponse(fh.read(), content_type="application/force-download")
                response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
                return response
        else:
            return HttpResponse('<html><h3>Berkas tidak ditemukan</h3></html>')
    except ObjectDoesNotExist:
        return HttpResponse('<html><h3>Berkas tidak ditemukan</h3></html>')


def download_acc(cyd_id):
    try:
        app_id = Application.objects.get(cyd=cyd_id).id
        file_path = AcceptanceLetter.objects.get(application=app_id).document.path
        if os.path.exists(file_path):
            with open(file_path, 'rb') as fh:
                response = HttpResponse(fh.read(), content_type="application/force-download")
                response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
                return response
        else:
            return HttpResponse('<html><h3>Acceptance letter belum tersedia</h3></html>')
    except ObjectDoesNotExist:
        return HttpResponse('<html><h3>Acceptance letter belum tersedia</h3></html>')
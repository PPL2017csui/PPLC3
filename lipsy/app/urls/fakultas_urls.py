"""
URL for fakultas
"""
from django.conf.urls import url
from django.contrib import admin
import app.views.fakultas_views as fv

urlpatterns = [
	url(r'^view/active/?', fv.fakultas_pantau_aktif, name='fakultas_pantau_aktif'),
	url(r'^view/canceled/?', fv.fakultas_pantau_dibatalkan, name='fakultas_pantau_dibatalkan'),
	url(r'^view/accepted/?', fv.fakultas_pantau_disetujui, name='fakultas_pantau_disetujui'),
	url(r'^upload/new/?', fv.fakultas_upload, name="fakultas_upload"),
	url(r'^upload/revision/?', fv.fakultas_upload_revision, name="fakultas_upload_revision"),
]


"""
URL for dgb
"""
from django.conf.urls import url
from app.views import dgb_views as dv

urlpatterns = [
    url(r'^view/application_history/', dv.retrieve_application_history, name='dgb_application_history'),
]

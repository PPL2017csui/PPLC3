"""
URL for download file
"""
from django.conf.urls import url
from ..views import download_views as dv

urlpatterns = [
    url(r'^view/berkas(?P<cyd_id>[0-9]+)', dv.download_berkas, name='download_berkas'),
    url(r'^view/acc(?P<cyd_id>[0-9]+)', dv.download_acc, name='download_acc'),
]